package com.alkurop.bluetooth_le

import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothManager
import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import java.util.*

/**
 * Created by alkurop on 9/14/16.
 */
abstract class BaseBluetoothActivity : AppCompatActivity() {
    val BLUETOOTH_ENABLE_REQUEST = 333
    val UUID_SERVICE: UUID = UUID.fromString("1706BBC0-88AB-4B8D-877E-2237916EE929")
    val UUID_CHAR: UUID = UUID.randomUUID()
    val UUID_DESCRIPTOR: UUID = UUID.randomUUID()

    val bluetoothManager by lazy { getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager }
    val bluetoothAdapter by lazy { bluetoothManager.adapter }

    override fun onStart() {
        super.onStart()
        initBluetooth()
    }

    private fun initBluetooth() {

        if (!bluetoothAdapter.isEnabled) {
            startActivityForResult(Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE), BLUETOOTH_ENABLE_REQUEST)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == BLUETOOTH_ENABLE_REQUEST) {
            initBluetooth()
        }
        super.onActivityResult(requestCode, resultCode, data)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }
}