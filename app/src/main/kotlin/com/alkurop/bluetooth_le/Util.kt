package com.alkurop.bluetooth_le

import android.app.Activity
import android.util.Log
import android.widget.Toast

/**
 * Created by alkurop on 9/14/16.
 */
fun Activity.toast(message: String): Unit {
    Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    Log.d("TOAST", message)
}