package com.alkurop.bluetooth_le.central

import android.bluetooth.*
import android.bluetooth.le.*
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.ParcelUuid
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import com.alkurop.bluetooth_le.BaseBluetoothActivity
import com.alkurop.bluetooth_le.R
import com.alkurop.bluetooth_le.toast
import com.github.alkurop.permissionmanager.PermissionOptionalDetails
import com.github.alkurop.permissionmanager.PermissionsManager
import kotlinx.android.synthetic.main.activity_central.*
import java.util.*

/**
 * Created by alkurop on 9/14/16.
 */
class CentralActivity : BaseBluetoothActivity() {
    val permissionManager by lazy { PermissionsManager(this) }
    val scannerCallback = object : ScanCallback() {
        override fun onScanFailed(errorCode: Int) {
            toast("scan failed CODE-$errorCode")
        }

        override fun onScanResult(callbackType: Int, result: ScanResult?) {
            processScanResult(result)
        }

        override fun onBatchScanResults(results: MutableList<ScanResult>?) {
            toast("batch scan result received")
        }
    }
    var gatt: BluetoothGatt? = null
    private val adapter = AdapterCentral { connectToDevice(it) }

    private fun connectToDevice(it: BluetoothDevice) {
         gatt = bluetoothAdapter.getRemoteDevice(it.address).connectGatt(this, false, object : BluetoothGattCallback() {
            override fun onConnectionStateChange(gatt: BluetoothGatt?, status: Int, newState: Int) {
                runOnUiThread { toast(
                          "onConnectionStateChange status ${status} newstate ${newState}"
                )
                    Log.d("CentralActivity","onConnectionStateChange status ${status} newstate ${newState}")}
                if(newState == BluetoothProfile.STATE_CONNECTED) {
                    gatt?.discoverServices();
                }else {
                    gatt?.close();
                }

            }

            override fun onServicesDiscovered(gatt: BluetoothGatt?, status: Int) {
                runOnUiThread { toast("onServicesDiscovered") }
                if (status == BluetoothGatt.GATT_SUCCESS) {
                    //get all services and show in list
                    displayGattServices(gatt?.getServices());
                } else {
                    runOnUiThread { toast("onServicesDiscovered received: " + status) }
                }

            }


            override fun onCharacteristicRead(gatt: BluetoothGatt?, characteristic: BluetoothGattCharacteristic?, status: Int) {
                runOnUiThread {
                    toast("onCharacteristicRead")
                    toast("characteristic changer ${characteristic?.value}")
                }
               /* gatt?.setCharacteristicNotification(characteristic, true)
                val descriptor = characteristic?.descriptors?.get(0)
                descriptor?.value = BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE
                if (descriptor != null) {
                    gatt?.writeDescriptor(descriptor)
                }*/
            }

            override fun onCharacteristicChanged(gatt: BluetoothGatt?, characteristic: BluetoothGattCharacteristic?) {
                runOnUiThread { toast("onCharacteristicChanged") }
            }
        },1)

    }

    private fun displayGattServices(services: List<BluetoothGattService>?) {
        toast("servises discocered count ${services?.size}")
    }

    private fun parseData(scanRecords: Map<ParcelUuid, ByteArray>): MutableList<ScanData> {
        AdvertiseData.Builder().build()
        val records = mutableListOf<ScanData>()
        scanRecords.forEach { it ->
            records.add(ScanData(it.value.size, String(it.value), it.key.uuid.toString()))
        }

        return records
    }

    private fun processScanResult(result: ScanResult?) {
        val res = result
        if (res != null && res.device != null && res.scanRecord != null)
            with(res) {
                val name = device.name
                val rssi = rssi
                val data = parseData(scanRecord.serviceData)

                val info = StringBuilder()
                          .append("adress ${device.address}").append("\n")
                          .toString()
                val advertiser = Advertiser(name, rssi, data, info, device)
                adapter.addItem(advertiser)
            }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_central)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        recycler.layoutManager = LinearLayoutManager(this)
        recycler.adapter = adapter

        brnDiscover.setOnClickListener {
            askForPermissions { discoverAdvertisers() }
        }
    }

    private fun discoverAdvertisers() {
        adapter.clearItems()
        bluetoothAdapter.bluetoothLeScanner.stopScan(scannerCallback)
        Thread({
            val filter = ScanFilter.Builder().setServiceUuid(ParcelUuid(UUID_SERVICE)).build()
            val filters = mutableListOf<ScanFilter>()
            filters.add(filter)
            val settings = ScanSettings.Builder().setScanMode(ScanSettings.SCAN_MODE_LOW_LATENCY).build()
            bluetoothAdapter.bluetoothLeScanner.startScan( scannerCallback)

        }).start()
        Handler().postDelayed({
            bluetoothAdapter.bluetoothLeScanner.stopScan(scannerCallback)
        }, 10000)
    }

    private fun askForPermissions(onPermissinsGranted: () -> Unit) {
        val permissionsMap = HashMap<String, PermissionOptionalDetails>()
        permissionsMap.put(android.Manifest.permission.ACCESS_FINE_LOCATION,
                  PermissionOptionalDetails("Location permission", "Required to scan Bluetooth"))
        permissionManager.addPermissions(permissionsMap)
        permissionManager.addPermissionsListener {
            permissionManager.clearPermissionsListeners()
            permissionManager.clearPermissions()
            it.forEach {
                if (!it.value) {
                    return@addPermissionsListener
                }
            }
            onPermissinsGranted.invoke()
        }
        permissionManager.makePermissionRequest()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        permissionManager.onActivityResult(requestCode)
        super.onActivityResult(requestCode, resultCode, data)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        permissionManager.onRequestPermissionsResult(requestCode, permissions, grantResults)
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    override fun onDestroy() {
        super.onDestroy()
        gatt?.disconnect()
        gatt?.close()
    }
}


