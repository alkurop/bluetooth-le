package com.alkurop.bluetooth_le.central

import android.bluetooth.BluetoothDevice
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.alkurop.bluetooth_le.R
import kotlinx.android.synthetic.main.list_central_item.view.*

/**
 * Created by alkurop on 9/15/16.
 */
class CentralVH(itemView: View, onItemClicked: (Int) -> Unit) : RecyclerView.ViewHolder(itemView) {
    init {
        itemView.container.setOnClickListener { onItemClicked.invoke(adapterPosition) }
    }

    fun bindView(advertiser: Advertiser) {
        with(advertiser) {
            itemView.name.text = name
            itemView.rssi.text = "$rssi DB"
            val dataBuilder = StringBuilder()
            dataList.forEach { data ->
                dataBuilder
                          .append(data.data).append("\n")
                          .append("length ${data.length}").append("\n")
                          .append("uuid ${data.uuid}").append("\n")
                          .append("\n")
                          .append("\n")
            }
            itemView.data.text = dataBuilder.toString()
            itemView.info.text = info
        }
    }
}

class AdapterCentral(val onAdvertiserClicked: (BluetoothDevice) -> Unit) : RecyclerView.Adapter<CentralVH>() {
    val advertisers = mutableListOf<Advertiser>()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CentralVH {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.list_central_item, parent, false);
        return CentralVH(view, { onAdvertiserClicked(advertisers[it].device) })
    }

    override fun onBindViewHolder(holder: CentralVH?, position: Int) {
        holder?.bindView(advertisers[position])
    }

    override fun getItemCount(): Int =
              advertisers.size

    fun addItem(advertiser: Advertiser) {
         advertisers.add(advertiser)
        notifyDataSetChanged()
    }

    fun clearItems() {
        advertisers.clear()
        notifyDataSetChanged()
    }
}

data class ScanData(val length: Int, val data: String, val uuid: String)

class Advertiser(val name: String?, val rssi: Int, val dataList: List<ScanData>, val info: String, val device: BluetoothDevice) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is Advertiser) return false

        if (name != other.name) return false
        if (dataList != other.dataList) return false
        if (info != other.info) return false

        return true
    }

    override fun hashCode(): Int {
        var result = name?.hashCode() ?: 0
        result = 31 * result + dataList.hashCode()
        result = 31 * result + info.hashCode()
        return result
    }
}