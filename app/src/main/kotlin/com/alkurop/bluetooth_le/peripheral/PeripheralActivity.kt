package com.alkurop.bluetooth_le.peripheral

import android.bluetooth.*
import android.bluetooth.le.AdvertiseCallback
import android.bluetooth.le.AdvertiseData
import android.bluetooth.le.AdvertiseSettings
import android.bluetooth.le.BluetoothLeAdvertiser
import android.os.Bundle
import android.os.ParcelUuid
import android.widget.Toast
import com.alkurop.bluetooth_le.BaseBluetoothActivity
import com.alkurop.bluetooth_le.R
import com.alkurop.bluetooth_le.toast
import com.github.alkurop.mvpbase.utils.hideKeyboard
import kotlinx.android.synthetic.main.activity_peripheral.*
import java.nio.charset.Charset

/**
 * Created by alkurop on 9/14/16.
 */
class PeripheralActivity : BaseBluetoothActivity() {
    private val devices = mutableListOf<BluetoothDevice>()
    var gatServer: BluetoothGattServer? = null
    var bluetoothAdvertiser: BluetoothLeAdvertiser? = null
    var adCallback = object : AdvertiseCallback() {
        override fun onStartSuccess(settingsInEffect: AdvertiseSettings?) {
            toast("Started advertising")
        }

        override fun onStartFailure(errorCode: Int) {
            toast("Failed to advertise CODE$errorCode")
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_peripheral)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        btnAdvertise.setOnClickListener {
            hideKeyboard()
            val advertiseText = advertiseMsg.text.toString().trim()
            if (advertiseText.length > 0) {
                stopAdvertising()
                advertise(advertiseText)
            } else {
                Toast.makeText(this, "enter text", Toast.LENGTH_SHORT).show()
            }
        }
        btnNorify.setOnClickListener {
            toast("notify")
            val advertiseText = advertiseMsg.text.toString().trim()
            devices.forEach {
                notifyDevice(it, advertiseText)
            }
        }
        startGatt()

    }

    private fun stopAdvertising() {
        bluetoothAdvertiser?.stopAdvertising(adCallback)
    }

    private fun advertise(advertiseText: String) {
        val settings = AdvertiseSettings.Builder()
                  .setAdvertiseMode(AdvertiseSettings.ADVERTISE_MODE_LOW_POWER)
                  .setTxPowerLevel(AdvertiseSettings.ADVERTISE_TX_POWER_MEDIUM)
                  .setConnectable(false)
                  .build()

        val data = AdvertiseData.Builder()
                  .setIncludeDeviceName(false)
                  .addServiceUuid(ParcelUuid(UUID_SERVICE))
                  .addServiceData(ParcelUuid(UUID_SERVICE), advertiseText.toByteArray(Charset.forName("UTF-8")))
                  .build()

        bluetoothAdvertiser = bluetoothAdapter.bluetoothLeAdvertiser
        if (bluetoothAdvertiser == null) {
            toast("LE advertising not supported")
        } else {
            bluetoothAdvertiser?.startAdvertising(settings, data, adCallback)
        }
        initService()

    }

    private fun startGatt( ) {
        gatServer = bluetoothManager.openGattServer(this, object : BluetoothGattServerCallback() {
            override fun onConnectionStateChange(device: BluetoothDevice?, status: Int, newState: Int) {
                runOnUiThread { toast("conn state $status  $newState")}
                if (newState == BluetoothProfile.STATE_CONNECTED && device != null) {
                    devices.add(device)
                    runOnUiThread { toast("device connected")}
                } else if (newState == BluetoothProfile.STATE_DISCONNECTED) {
                    devices.remove(device)
                    runOnUiThread { toast("device disconnected")}

                }
            }

            override fun onCharacteristicReadRequest(device: BluetoothDevice?, requestId: Int, offset: Int, characteristic: BluetoothGattCharacteristic?) {
                runOnUiThread { toast("onCharacteristicReadRequest")}
                    gatServer?.sendResponse(device, requestId, BluetoothGatt.GATT_SUCCESS, 0,
                              "privet".toByteArray())

            }

            override fun onDescriptorWriteRequest(device: BluetoothDevice?, requestId: Int, descriptor: BluetoothGattDescriptor?, preparedWrite: Boolean, responseNeeded: Boolean, offset: Int, value: ByteArray?) {
                if (device != null) devices.add(device)

                if (responseNeeded) {
                    gatServer?.sendResponse(device, requestId, BluetoothGatt.GATT_SUCCESS, 0, value)
                }
                runOnUiThread { toast("onDescriptorWriteRequest")}

            }

            override fun onServiceAdded(status: Int, service: BluetoothGattService?) {
                runOnUiThread { toast("added gat service") }
            }
        })

    }

    override fun onDestroy() {
        super.onDestroy()
        stopAdvertising()
        gatServer?.close()
    }


    private fun initService() {
        val service = BluetoothGattService(UUID_SERVICE, BluetoothGattService.SERVICE_TYPE_SECONDARY)

        val characteristic = BluetoothGattCharacteristic(UUID_CHAR,
                  BluetoothGattCharacteristic.PROPERTY_READ or
                  BluetoothGattCharacteristic.WRITE_TYPE_DEFAULT or
                  BluetoothGattCharacteristic.PROPERTY_BROADCAST or
                  BluetoothGattCharacteristic.PROPERTY_WRITE or
                  BluetoothGattCharacteristic.PROPERTY_INDICATE or
                            BluetoothGattCharacteristic.PROPERTY_NOTIFY,
                  BluetoothGattCharacteristic.PERMISSION_READ or
                  BluetoothGattCharacteristic.PROPERTY_WRITE

        )

        val descriptor = BluetoothGattDescriptor(UUID_DESCRIPTOR,
                  BluetoothGattDescriptor.PERMISSION_READ  or
                  BluetoothGattDescriptor.PERMISSION_WRITE
        )

        characteristic.addDescriptor(descriptor)
        service.addCharacteristic(characteristic)
        gatServer?.addService(service)
    }
    private fun notifyDevice(device: BluetoothDevice, text:String) {

        val characteristic = gatServer?.getService(UUID_SERVICE)?.getCharacteristic(UUID_CHAR)
        characteristic?.setValue(text)

        gatServer?.notifyCharacteristicChanged(device, characteristic, false)
    }
}