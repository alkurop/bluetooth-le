package com.alkurop.bluetooth_le

import android.bluetooth.BluetoothAdapter
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.alkurop.bluetooth_le.central.CentralActivity
import com.alkurop.bluetooth_le.peripheral.PeripheralActivity
import kotlinx.android.synthetic.main.activity_main.*

/**
 * Created by alkurop on 9/14/16.
 */
class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initButtons()
    }

    private fun checkBluetooth() {
        if (!packageManager.hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) {
            toast("LE not supported.")
            finish()
        }

        if (!BluetoothAdapter.getDefaultAdapter().isMultipleAdvertisementSupported) {
            toast("advertisement not support")
        }

    }

    override fun onStart() {
        super.onStart()
        checkBluetooth()
    }

    private fun initButtons() {
        btnCentral.setOnClickListener {
            startActivity(Intent(this, CentralActivity::class.java))
        }
        btnPeripheral.setOnClickListener {
            startActivity(Intent(this, PeripheralActivity::class.java))
        }
    }


}